import sys
import os
from PyQt5.QtWidgets import QApplication, QFileDialog, QMainWindow
import mainwindow,imagedisplay
from psutil import virtual_memory

version_number = 0.2

filesuffix = [".tif",".TIF",".tiff",".TIFF"]
mem = virtual_memory()
mem_gb = round(mem.total/1073741824,1)

enoughmemory = False
if mem_gb > 32.0:
    enoughmemory = True
class ExampleApp(QMainWindow, mainwindow.Ui_MainWindow):
    def __init__(self, parent=None):
        super(ExampleApp, self).__init__(parent)
        self.setupUi(self)
        self.setWindowTitle("TIFF Merger " + str(version_number))
        self.btnBrowse_i.clicked.connect(self.browse_folder_i)
        self.btnBrowse_o.clicked.connect(self.browse_folder_o)
        self.quitbutton.clicked.connect(self.quitd)
        if not enoughmemory:
            self.textstatus.append("WARNING: WAT Algorithm is NOT RECOMMENDED on computers with less than 32 GB memory")
            self.textstatus.append("You have " + str(mem_gb) + " GB of memory")
    def browse_folder_i(self):
        self.filelist.clear()
        directory = QFileDialog.get(self, "Pick input stack")

        if directory:
            self.lineEdit_1.setText(directory)
            for file_name in os.listdir(directory):
                if file_name.endswith(tuple(filesuffix)):
                    self.filelist.addItem(file_name)

                numfiles = len(self.filelist)
            self.textstatus.append(str(numfiles) + " selected for merge")
    def browse_folder_o(self):
        directory = QFileDialog.getExistingDirectory(self, "Pick output/processed directory")
        if directory:
            self.lineEdit_2.setText(directory)

    def quitd(self):
        sys.exit("Aborting...")




def main():
    app = QApplication(sys.argv)
    form = ExampleApp()
    form.show()
    app.exec_()


if __name__ == '__main__':
    main()

#build pyinstaller.py merge_withgui.py
#pyinstaller merge_withgui.py (?)